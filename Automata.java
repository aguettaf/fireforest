/**
 * @author Abdelkader_Guettaf
 * 
 */

package fireForest ;
import java.util.Random;

public class Automata {
	
	/**
	 * this Java file create an example of the spread 
	 * of fire in a forest, using the Cellular Automaton method
	 */

	
	/**the attributes: 
	 * 
	 * matrix: an array of integers with private visibility
	 * dimension an integer with private visibility
	 * 
	 * 
	 */
	
	 private int[][] matrix;
	 private int dimension;
	

	 /**the getters : 
	  * 
	  * getmatrix(int i,int j)
	  * 
	  * to access to a specific element in the array
	  * 
	  * @param i: for the line index
	  * @param j: for the column index
	  * @return matrix [i][j]: an element of the array
	  */

	  public int getmatrix(int i,int j) {
		 return this.matrix[i][j];
	  }

	  /** 
	   * getdimension ()
	   * 
	   * to access the dimension of our array
	   * 
	   * @return the size of our array
	   */

	  public int getdimension() {
		  return this.dimension;
	  }

		  
	  /**the setters : 
	   * 
	   * setmatrix(int i, int j, int nouvelleValeur)
	   * 
	   * to change the value of each item
	   * 
	   * @param i the line index
	   * @param j the column index
	   */

	  public void setmatrix(int i, int j, int nouvelleValeur) {
		  this.matrix[i][j]=nouvelleValeur;
	  }

		  
	  /** 
	   * setdimension (int dimension)
	   * 
	   * to change the size of our array
	   * 
	   * @param dimension 
	   */

	  public void setdimension(int dimension) {
		  this.dimension=dimension;
	  }
	
		  

	  /**Constructor: Automata (int n, double p)
	   * 
	   * polymorphism:
	   * @see Automata #Automata(double)
	   * 
	   * to create an array: specify the dimension and fill the array with 0 or 1
	   * according to the density entered by the user
	   * 
	   * @param int n:the dimensions entered by the user
	   * @param double p: percentage of density entered by the user
	   */
 

	  public Automata(int n, double p) {
		  setdimension(n);
		  matrix = new int[getdimension()][getdimension()];
		  for (int i=0;i<getdimension();i++) {
			  for (int j=0;j<getdimension();j++) {
				  double alea = Math.random();
				  if (alea<p) {
					  setmatrix(i,j,1) ;
				  }else {
					  setmatrix(i,j,0) ;
				  }	
			  }
		  }
	  }

	
	
	  /**Constructor : Automata (double p) 
	   * 
	   * polymorphism:
	   * @see Automata #Automata(int, double)
	   * 
	   * construction of an array with a fixed dimension 10
	   * 
	   * @param p: user-entered density
	   * 
	   * output: we called the Automata (n, p) constructor with fixing n in 10
	   */

	  public Automata (double p){
		  this(10,p);
	  }


	  /**the method: forestDisplay()
	   * 
	   * Display array as:
	   * ".": empty cell, "T": a tree, "O":Tree on fire, "_":Tree dead, 
	   * 
	   */
 

	  public void forestDisplay() {
		  for (int i=0;i<getdimension();i++) {
			  System.out.print("\n");
			  for (int j=0;j<getdimension();j++) {
				  	if(getmatrix(i,j)==0) {System.out.print(".");}
				  	else if(getmatrix(i,j)==1) {System.out.print("T");}
				  	else if(getmatrix(i,j)==5) {System.out.print("O");}		
				  	else if(getmatrix(i,j)==-1) {System.out.print("_");}	
			  }	
		  }
	  }


	  /**the method : isRazed ()
	   * 
	   * to check that the forest is completely razed
	   * 
	   * @return boolean: 
	   * false: the presence of a single tree or tree on fire, 
	   * true: there is no tree
	   */

	   public boolean isRazed() {
		   for (int i=0;i<getdimension();i++) {
			   for (int j=0;j<getdimension();j++) {
				   if ((getmatrix(i,j)==1)|| (getmatrix(i,j)==5)) {return true ;}
			   }
		   }
		   return false;
	   }


	  /**the method: isOnFire ()
	   * 
	   * polymorphism:
	   * @see Automata #isOnFire(int, int)
	   * 
	   * 
	   * public: public use
	   * check the presence of a burning tree throughout the forest		  
	   *  
	   *  
	   * @return boolean: 
	   * true: the presence of a single tree on fire, 
	   * false: no tree on fire
	   */

	   public boolean isOnFire() {
		   for (int i=0;i<getdimension();i++) {
			   for (int j=0;j<getdimension();j++) {
				   if (getmatrix (i,j)==5) {return true;}
			   }
		   }
		   return false;
	   }



	  /**the method : putFire (i,j) 
	   * 
	   * polymorphism: according to signature:
	   * @see Automata #putFire()
	   * 
	   * generate a fire in a tree according to the parameters entered by the user (i, j)
	   * 
	   * @param int i :line index
	   * @param int j :column index
	   */

	  public void putFire(int i, int j) {
		  if (i>(getdimension()-1) || j> (getdimension()-1) || i<0 || j<0){System.out.println ("you have to choose numbers inside dimensions");}
		  else{setmatrix(i,j,5);}
	  }	


	  /**the method putFire()
	   *  
	   * polymorphism
	   * @see Automata #putFire(int, int)
	   * 
	   * 
	   * Generate fire randomly in a tree in the forest
	   * 
	   * NB: objetAlea.nextInt(getdimension()): pull a integer number randomly,
	   * between 0 (included) and the length of the array (dimension-1)
	   * 
	   */

	  public void putFire() {
		  Random objetAlea=new Random ();
		  int i=objetAlea.nextInt(getdimension());
		  int j=objetAlea.nextInt(getdimension());
		  setmatrix(i,j,5);
	  }



	  /**3.2 SPRIDING_OF_THE_FIRE
	   * 
	   * 
	   * the method isTree(i,j)
	   * check the presence of a tree in a specific place according to the parameters entered	
	   * 
	   * private: local use
	   * 
	   * 
	   * @param int i : line index 
	   * @param int j : column index 
	   * @return boolean true: there is a tree, false: no tree
	   */

	  private boolean isTree (int i, int j) {
		  return (getmatrix(i,j)==1);				
	  }


	  /**the method isOnFire (int i, int j) 
	   * 
	   * polymorphism signature (i,j)
	   * @see Automata #isOnFire()
	   * 
	   * private: local use
	   * check for the presence of a burning tree in a specific cell (i, j)
	   * 
	   * 
	   * @param int i: line index 
	   * @param int j: column index
	   * @return boolean : true: there is a tree on fire, false: no tree on fire
	   */

	  private boolean isOnFire (int i, int j) {
		  return (getmatrix (i,j)==5) ;	
	  }

			
		
	  /**the method hasNeihborOnFire(int i, int j)
	   * 
	   * 
	   * the first condition (if) in each block ensures that we will not check the neighbors of the edges of our array (which do not exist)
	   * second condition: check if there is one or more neighbors on fire of a specific cell, using the isOnFire method (i, j)
	   * 
	   * 
	   * @param int i :line index of a cell
	   * @param int j : column index of a cell
	   * 
	   * @return boolean: true : there is a neighbor on fire, false: no neighbor on fire
	   * 
	   */
		  

	  private boolean hasNeihborOnFire(int i, int j) {

		   if (i>0) {
				if (isOnFire(i-1, j)) {
					return true ;
				}
			}
			if (i<(getdimension()-1)) {
				if (isOnFire(i+1,j)) {
					return true ;
				}
			}
			if (j>0) {
				if (isOnFire(i, j-1)) {
					return true ;
				}
			}
			if (j<(getdimension()-1)) {
				if (isOnFire(i,j+1)) {
					return true;
				}
			}
			return(false); 
	  }



	  /** the method nextState(int i, int j)
	   * 
	   * 
	   * represents the following state of a precise cell (i, j):
	   * if we have a burning tree "O / 5" it will become ash "_ / - 1" and if we have
	   * a tree "T / 1" and one of these neighbors on fire, it will become on fire "O / 5"
	   * 
	   * 
	   * @param int i line index 
	   * @param int j column index
	   * 
	   * @return an integer matrix [i][j]: which represents new state of the tree "5 / O" or "-1 / _"
	   */


	  private int nextState(int i, int j) {
		  if (isOnFire(i,j)) 
		  {return (-1);}
		  else if (isTree(i,j) && hasNeihborOnFire(i, j))
		  {return (5);}
		  return matrix [i][j];
	  }



	  /**the method propagateFire1() 
	   * 
	   * 
	   * it will apply the changes of nextState on the whole matrix of the forest
	   * we create a new nextState matrix with the same matrix dimensions
	   * and we fill this matrix with the new finite matrix nextState
	   * 
	   * 
	   * output: void (it creates the matrix which will take the nextState values)
	   * 
		**/


	  private void propagateFire1() {
		  int[][] nextState = new int[getdimension()][getdimension()] ;
		  for (int i=0;i<getdimension();i++) {
			  for (int j=0;j<getdimension();j++) {
				  nextState[i][j] = nextState(i,j) ;
			  }
		  }
		  matrix = nextState ;
	  }



	  /**3.6 Spreading over a long period
	   * 
	   * the method  propagateFire(int n)
	   *  
	   * polymorphism
	   * @see #propagateFire()
	   * 
	   * displays new forest states in a user-defined time per hour (n)
	   * 
	   * @param int n: number of hours entered by the user
	   */
	
	
	  public void propagateFire(int n) {
		  for (int h=0;h<=n;h++){ 
			  System.out.println("\nForest in time "+h);
			  propagateFire1();
			  forestDisplay();
			  
			  /**pause between display of matrices*/
			  try {Thread.sleep (5000);}
			  catch (InterruptedException ex) 
			  {Thread.currentThread().interrupt();}
		  }
	  }
	
		  
	  /**the method propagateFire() 
	   * 
	   * polymorphism
	   * @see #propagateFire(int)
	   * 
	   * output: the time in hours until the total fire extinguished and the status display after each hour
	   * we use "while" because we don't know the time limit of this operation (until the isOnFire condition become false)
	   *
	   */

		  
	  public void propagateFire() {
		  int counter = 0;
		  while (isOnFire()) {
			  counter++;
			  System.out.println("\nForest in time "+counter);
			  propagateFire1();
			  forestDisplay();
				  
			  /**pause between display of matrixes*/
			  try {Thread.sleep (5000);}
			  catch (InterruptedException ex) 
			  {Thread.currentThread().interrupt();}	
		  }
	  }	
		  
		  
	  /**Bonus 1 
	   * 
	   * the method hasNeihborOnFireBonus1(int i, int j)
	   * 
	   * check if there is a fire in all the neighbors (diagonal) to use it after in nextState
	   * 
	   */
		  
	  public boolean hasNeihborOnFireBonus1(int i, int j) {

		  if ((i>0) && (j>0) && (i<(getdimension()-1)) && (j<(getdimension()-1))){
			  if (isOnFire (i,j-1)|| 
				  isOnFire (i,j+1)|| 
				  isOnFire (i-1,j)|| 
				  isOnFire (i+1,j)||
				  isOnFire (i-1,j+1)||
				  isOnFire (i+1,j+1)||
				  isOnFire (i-1,j-1)||
				  isOnFire (i+1,j-1))
			  {return true;}	
		  }
		  return false ;
	  }
		  
		  
	  /**the method windEffect ()
	   * 
	   *  it will generate a wind where the direction depends
	   *  to a random number between 0 (included) and 4 (not included), 
	   * 
	   * */
		  
	  public void windEffect() {
		  
		 Random o=new Random();
		 int alea=o.nextInt(4); 
			 
		/**a wind from the left to the right*/
			 
		 if(alea==0) {
			 for (int j=0;j<getdimension()-1;j++) {
				 for (int i=0;i<getdimension()-1;i++) {
					 if (isOnFire(i,j)) {
						 if (isTree(i,j+1)) {
							 putFire(i,j+1);
						 }
					 }
				 }
			 }
				 
				 
		/**a wind from the right to the left*/
				 
				 
		 }else if(alea==1) {
			 for (int j=getdimension()-1;j<0;j--) {
				 for (int i=0;i<getdimension()-1;i++) {
					 if (isOnFire(i,j)) {
						 if (isTree(i,j-1)) {
							 putFire(i,j-1);
						 }
					 }
				 }
			 }
				 
				 
		/**a wind from the top to the bottom*/
				 
				 
	     }else if(alea==2) {
			 for (int i=0;i<getdimension()-1;i++) {
				 for (int j=0;j<getdimension()-1;j++) {
					 if (isOnFire(i,j)) {
						 if (isTree(i+1,j)) {
							 putFire(i+1,j);
						 }
					 }
				 }
			 }
			
				 
		/**a wind from the bottom to the top*/
				 
				 
	     }else if(alea==3) {
			 for (int i=getdimension()-1;i<0;i--) {
				 for (int j=0;j<getdimension()-1;j++) {
					 if (isOnFire(i,j)) {
						 if (isTree(i-1,j)) {
							 putFire(i-1,j);
						 }
					 }
				 }
			 }
	     }
	  }
		  
		  
	  public static void main(String[] args) {
		  Automata iAC = new Automata(0.5);
			System.out.println("Avant le feu :");
			iAC.forestDisplay();
			iAC.putFire(5,5);
			System.out.println("\nOn met le feu :");
			iAC.forestDisplay();
			
			iAC.propagateFire();

	  }
		  
}


